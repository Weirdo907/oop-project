package com.oop2.www;

public abstract class Person {
private String name;
private String surname;
private int age;
private String gender;
private int was_born;


public  Person(String name) {
	this.name = name;
	
  }

public void displayName( )  {
	      System.out.println("My name is" +name);
}
public String getName( ) {
	return name;
}
public void setName(String name) {
	this.name =name;
}
public String getSurname( ) {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public int getWas_born() {
	return was_born;
}

public void setWas_born(int was_born) {
	this.was_born = was_born;
	
}


}
