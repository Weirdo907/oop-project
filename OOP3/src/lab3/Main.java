package lab3;

public class Main {

	public static void main(String[] args) {
		Employee developer1 = new Developer("name1","surname1", 100);
		Employee developer2 = new Developer("name2", "surname2", 200);
		Employee developer3 = new Developer("name3", "surname", 300);
		
		Employee manager1 = new Manager("Tom","Smith",600,100);
		Employee manager2 = new Manager("Maja", "Smith",600,100);
		Employee manager3 = new Manager("Carl", "Jackson", 600,100);
		
		developer1.setAddress("country1","city1","street1",100);
		developer2.setAddress("country2","city2","street2", 200);
		developer3.setAddress("country3", "city3","street3", 300);
		
		manager1.setAddress("Poland1","Wroclaw","Pomorska",52);
		manager2.setAddress("Poland2","Wroclaw2","Pomorska2",52);
		manager3.setAddress("Poland3","Wroclaw3","Pomorska3",52);
		
		Developer deveoper1;
		((Manager)manager1).addSubordinates((Developer)developer1);
		Developer deveoper2;
		((Manager)manager2).addSubordinates((Developer)developer2);
		Developer deveoper3;
		((Manager)manager3).addSubordinates((Developer)developer3);
		
		System.out.println(developer1.toString());
		System.out.println(manager1.toString());
		System.out.println(manager2.toString());
		System.out.println(manager3.toString());
		
	}

}
