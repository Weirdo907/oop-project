package lab3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class testlab4oop {
	private Manager commonManager = new Manager("too", "far", 1000, 200);
	private Developer commonDeveloper = new Developer("far", "too", 800);
	
	@BeforeEach
	public void test() {
		commonManager = new Manager("too", "far", 1000, 200);
		commonDeveloper = new Developer("far", "too", 800);
		commonManager.setAddress("a", "a","a", 1);
		commonDeveloper.setAddress("b", "b", "b", 2);
		commonManager.addSubordinates(commonDeveloper);
	}
	
	@Test
	void testManagerSalary() {
		assertEquals(1200, commonManager.getSalary());
	}
	@Test
	void testDeveloperSalary() {
		assertEquals(800, commonDeveloper.getSalary ());
	}
	@Test
	void testAddSubordinate() {
		assertTrue(commonManager.getSubordinates().contains(commonDeveloper));
	}
	@Test
	void testManagerToString() {
		// when
		String displayManager = "Manager - too far, a, a, a 1, salary: 1200\r\n" +
		"SubordinateDeveloper - far too,b,b,b 2, salary : 800\r\n";
		//then
		assertEquals(displayManager, commonManager.toString());
	}
	
	@Test
	void testDeveloperToString() {
		// when
		String displayDeveloper = "Developer - far too, b, b, b 2 ,salary : 800\r\n" ;
		
		//Object dislpayDeveloper = null;
		//Object dislpayDeveloper = null;
		//then
		assertEquals(displayDeveloper, commonDeveloper.toString());
	}
	



	
	}


