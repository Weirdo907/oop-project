package lab5;

import java.util.ArrayList;
import java.util.List;

public class Customer extends Account {
	List<Item> myBasket = new ArrayList<>();

	public Customer(String username, String password) {
		super(username, password);
		// TODO Auto-generated constructor stub
	}

	public List<Item> getMyBasket() {
		return myBasket;
	}

	public void setMyBasket(List<Item> myBasket) {
		this.myBasket = myBasket;
	}

	@Override
	public String toString() {
		return "[Username: " + getUsername() + " Password: " + getPassword() + "] ";
	}

}
