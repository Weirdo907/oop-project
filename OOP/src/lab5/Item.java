package lab5;

public enum Item {
	

	ITEM1(15,0), ITEM2(17,0), ITEM3(25,0), ITEM4(16,0), ITEM5(35,0), ITEM6(19,0);

	private final int price;
	private int quantity;


	Item(int price, int quantity) {
		this.price = price;
		this.quantity = quantity;
	}


	public int getPrice() {
		return price;
	}




	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	

}



