package lab5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
	static String guestOption;
	static List<Admin> adminList = new ArrayList<>();
	static List<Customer> customerList = new ArrayList<>();
	static Item[] itemList = Item.values();
	static Customer currentCustomer;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Admin admin = new Admin("a", "a");
		adminList.add(admin);
		Customer customer = new Customer("c", "c");
		customerList.add(customer);
		while (true) {
			GuestMenu();
		}
	}

	public static void GuestMenu() {
		System.out.println("GUEST");
		System.out.println("1. Login");
		System.out.println("2. Register");
		System.out.println("3. Exit");
		System.out.print("Choose option: ");
		Scanner input = new Scanner(System.in);
		int option = input.nextInt();

		switch (option) {
		case 1:
			guestOption = "Login";
			GuestMenu2();
			break;
		case 2:
			guestOption = "Register";
			GuestMenu2();
			break;
		case 3:
			return;
		default:
			System.out.println("Invalid option!");

		}

	}

	public static void GuestMenu2() {
		System.out.println("1. Admin");
		System.out.println("2. Customer");
		System.out.print("Choose option: ");
		Scanner input = new Scanner(System.in);
		int option = input.nextInt();

		switch (option) {
		case 1:
			guestOption += "Admin";
			Form();
			break;
		case 2:
			guestOption += "Customer";
			Form();
			break;
		default:
			System.out.println("Invalid option!");

		}
	}

	public static void Form() {
		Scanner input = new Scanner(System.in);
		System.out.print("Username: ");
		String username = input.nextLine();
		System.out.print("Password: ");
		String password = input.nextLine();

		int index = getIndexAccount(username, password);

		switch (guestOption) {
		case "LoginAdmin":
			if (index == -1) {
				System.out.println("Username/password incorrect");

			} else {
				AdminMenu();
			}
			break;
		case "RegisterAdmin":
			if (!checkUsername(username)) {
				Admin newAdmin = new Admin(username, password);
				adminList.add(newAdmin);
				AdminMenu();
			} else {
				System.out.println("Username already exists!");
			}
			break;
		case "LoginCustomer":
			if (index == -1) {
				System.out.println("Username/password incorrect");
			} else {
				currentCustomer = customerList.get(index);
				CustomerMenu();
			}
			break;
		case "RegisterCustomer":
			if (!checkUsername(username)) {
				Customer newCustomer = new Customer(username, password);
				currentCustomer = newCustomer;
				customerList.add(newCustomer);
				CustomerMenu();
			} else {
				System.out.println("Username already exists!");

			}
			break;

		}
	}

	// get index of the account when login, return -1 if account doesn't exist
	public static int getIndexAccount(String username, String password) {
		if (guestOption.equals("LoginAdmin")) {
			for (int i = 0; i < adminList.size(); i++) {
				if (adminList.get(i).getUsername().equals(username)
						&& adminList.get(i).getPassword().equals(password)) {
					return i;
				}
			}
		} else {
			for (int i = 0; i < customerList.size(); i++) {
				if (customerList.get(i).getUsername().equals(username)
						&& customerList.get(i).getPassword().equals(password)) {
					return i;
				}
			}
		}
		return -1;
	}

	// check username exists, return true if exist
	public static boolean checkUsername(String username) {
		if (guestOption.equals("RegisterAdmin")) {
			for (int i = 0; i < adminList.size(); i++) {
				if (adminList.get(i).getUsername().equals(username)) {
					return true;
				}
			}
		} else {
			for (int i = 0; i < customerList.size(); i++) {
				if (customerList.get(i).getUsername().equals(username)) {
					return true;
				}
			}
		}
		return false;
	}

	public static void CustomerMenu() {
		while (true) {
			System.out.println("Hi customer "+currentCustomer.getUsername());
			System.out.println("CUSTOMER");
			System.out.println("1. Shopping");
			System.out.println("2. My basket");
			System.out.println("3. Log out");
			System.out.print("Choose option: ");

			Scanner input = new Scanner(System.in);
			int option = input.nextInt();

			switch (option) {
			case 1:
				ItemList();
				break;
			case 2:
				Basket();
				break;
			case 3:
				return;
			default:
				System.out.println("Invalid option!");

			}
		}

	}

	public static void ItemList() {
		while (true) {
			System.out.println("1. " + Item.ITEM1);
			System.out.println("2. " + Item.ITEM2);
			System.out.println("3. " + Item.ITEM3);
			System.out.println("4. " + Item.ITEM4);
			System.out.println("5. " + Item.ITEM5);
			System.out.println("6. " + Item.ITEM6);
			System.out.println("7. My basket");
			System.out.println("8. Return");
			System.out.print("Choose item: ");

			Scanner input = new Scanner(System.in);
			int option = input.nextInt();
			if (option == 7) {
				Basket();
			} else if (option == 8) {
				return;
			} else {
				System.out.print("Quantity: ");
				int quantity = input.nextInt();
				itemList[option - 1].setQuantity(quantity);
				currentCustomer.getMyBasket().add(itemList[option - 1]);

			}
		}

	}

	public static void Basket() {
		int total = 0;
		List<Item> basket = currentCustomer.getMyBasket();
		
		System.out.println("My basket: ");
		for(int i=0; i<basket.size();i++) {
			total+=basket.get(i).getQuantity()*basket.get(i).getPrice();
			System.out.print("["+ basket.get(i)+ ": "+basket.get(i).getQuantity()+"]");
		}
		System.out.println(" Total price: "+ total);
	}

	public static void AdminMenu() {
		while (true) {
			System.out.println("ADMIN");
			System.out.println("1. Customer list");
			System.out.println("2. Item list");
			System.out.println("3. Log out");
			System.out.print("Choose option: ");

			Scanner input = new Scanner(System.in);
			int option = input.nextInt();

			switch (option) {
			case 1:
				for (Customer customer : customerList) {
					System.out.print(customer);
				}
				System.out.println();
				break;
			case 2:
				System.out.println();
				break;
			case 3:
				return;
			default:
				System.out.println("Invalid option!");

			}
		}
	}

}
